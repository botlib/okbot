# setup.py
#
#

from setuptools import setup

def read():
    return open("README.rst", "r").read()

setup(
    name='okbot',
    version='1',
    url='https://bitbucket.org/botlib/okbot',
    author='Bart Thate',
    author_email='bthate@dds.nl', 
    description=""" the ok bot ! """,
    long_description=read(),
    long_description_content_type="text/x-rst",
    license='Public Domain',
    packages=["okbot"],
    namespace_packages=["okbot"],
    scripts=["bin/okbot", "bin/okbot", "bin/okcfg", "bin/okhup", "bin/oksh", "bin/okudp"],
    classifiers=['Development Status :: 3 - Alpha',
                 'License :: Public Domain',
                 'Operating System :: Unix',
                 'Programming Language :: Python',
                 'Topic :: Utilities'
                ]
)
