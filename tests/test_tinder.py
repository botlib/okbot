# OKBOT - the ok bot !
#
#

import os, random, sys, time, unittest
import okbot.tbl

from okbot.base import Object
from okbot.hdl import Command, Handler
from okbot.krn import get_kernel
from okbot.thr import launch

k = get_kernel()

param = Object()
param.ed = ["okbot.irc.Cfg", "okbot.rss.Cfg", "okbot.krn.Cfg", "okbot.irc.Cfg server localhost", "okbot.irc.Cfg channel \#dunkbots", "okbot.krn.Cfg packages okbot.]
param.dlt = ["reddit", ]
param.display = ["reddit title,summary,link",]
param.log = ["yo!", ""]
param.flt = ["0", "1", ""]
param.fnd = ["log yo", "todo yo", "rss reddit"]
param.meet = ["test@shell", "bart"]
param.rss = ["https://www.reddit.com/r/python/.rss", ""]
param.todo = ["yo!", ""]

class Command(Command):

    def show(self):
        for txt in self._result:
           print(txt)

class Test_Tinder(unittest.TestCase):

    def test_all(self):
        thrs = []
        for x in range(100):
            thrs.append(launch(tests, k))
        for thr in thrs:
            thr.join()

def consume(elems):
    fixed = []
    for e in elems:
        e.wait()
        fixed.append(e)
    for f in fixed:
        try:
            elems.remove(f)
        except ValueError:
            continue
        
def tests(b):
    events = []
    keys = list(okbot.tbl.names.keys())
    random.shuffle(keys)
    for cmd in keys:
        events.extend(do_cmd(k, cmd))
    consume(events)

def do_cmd(b, cmd):
    exs = param.get(cmd, [])
    if not exs:
        exs = ["bla",]
    e = list(exs)
    random.shuffle(e)
    events = []
    for ex in e:
        txt = cmd + " " + ex
        e = Command(txt, repr(k), "test@shell")
        k.put(e)
        events.append(e)
    return events
