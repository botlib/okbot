# OKBOT - the okbot !
#
#

import os

from okbot.base import workdir
from okbot.db import Db
from okbot.exp import ENOCLASS
from okbot.krn import get_kernel
from okbot.typ import get_cls

def __dir__():
    return ("ed",)

k = get_kernel()

def list_files(wd):
    return "|".join([x for x in os.listdir(os.path.join(wd, "store"))])

def ed(event):
    if not event.args:
        event.reply(list_files(workdir) or "no files yet")
        return
    cn = event.args[0]
    shorts = k.find_shorts("okbot")
    cn = shorts.get(cn, cn)    
    db = Db()
    l = db.last(cn)
    if not l:     
        try:
            c = get_cls(cn)
            l = c()
            event.reply("created %s" % cn)
        except ENOCLASS:
            event.reply(list_files(workdir) or "no files yet")
            return
    if len(event.args) == 1:
        event.reply(l)
        return
    if len(event.args) == 2:
        event.reply(l.get(event.args[1]))
        return
    setter = {event.args[1]: event.args[2]}
    l.edit(setter)
    p = l.save()
    event.reply("okbot.%s" % p)
