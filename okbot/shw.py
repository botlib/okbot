# OKBOT - the ok bot !
#
#

import os, pkg_resources, threading, time, okbot.tbl

from okbot.base import Object, starttime, __version__
from okbot.krn import get_kernel
from okbot.tms import elapsed
from okbot.typ import get_type

def __dir__():
    return ("cfg", "cmds", "fleet", "ps", "up", "v")

k = get_kernel()

def cmds(event):
    event.reply("|".join(sorted(okbot.tbl.names.keys())))

def ps(event):
    psformat = "%-8s %-50s"
    result = []
    for thr in sorted(threading.enumerate(), key=lambda x: x.getName()):
        if str(thr).startswith("<_"):
            continue
        d = vars(thr)
        o = Object()
        o.update(d)
        if o.get("sleep", None):
            up = o.sleep - int(time.time() - o.state.latest)
        else:
            up = int(time.time() - starttime)
        result.append((up, thr.getName(), o))
    nr = -1
    for up, thrname, o in sorted(result, key=lambda x: x[0]):
        nr += 1
        res = "%s %s" % (nr, psformat % (elapsed(up), thrname[:60]))
        if res.strip():
            event.reply(res)

def up(event):
    event.reply(elapsed(time.time() - starttime))

def v(event):
    event.reply("%s %s" % ("OKBOT", __version__))
