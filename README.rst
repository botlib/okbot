.. title:: no copyright, no LICENSE, placed in the public domain

Welcome to OKBOT, the ok bot ! see https://pypi.org/project/okbot/ ;]

OKBOT
#####

OKBOT  can fetch RSS feeds, lets you program your own commands, can work as a UDP to IRC
relay, has user management to limit access to prefered users and can run as a service to let
it restart after reboots. OKBOT is the result of 20 years of programming bots, was there 
in 2000, is here in 2020, has no copyright, no LICENSE and is placed in the Public Domain. 
This makes OKBOT truely free (pastable) code you can use how you see fit, i hope you enjoy 
using and programming OKE till the point you start programming your own bots yourself.

Have fun coding ;]

|

U S A G E
=========

::

 usage: 

   > okbot			- starts a service bot
   > okcmd         		- executes a command
   > okcmd cmds			- shows list of commands
   > okcfg 			- set <server> <channel> <nick> <owner>
   > okirc			- userland irc client bot.
   > oksh			- shell to system daemon
   > okhup			- restart service
   > okudp			- echo txt to irc channel
   > oku				- user version of the bot

 example:

   > okirc irc.freenode.net \#dunkbots okbot ~dunker@jsonbot/daddy


I N S T A L L
=============

you can download with pip3 and install globally:

::

 > sudo pip3 install okbot

You can also download the tarball and install from that, see https://pypi.org/project/okbot/#files

::

 > sudo python3 setup.py install

or install locally from tarball as a user:

::

 > sudo python3 setup.py install --user

if you want to develop on the bot clone the source at bitbucket.org:

::

 > git clone https://bitbucket.org/botlib/okbot

S E R V I C E
=============

if you want to run the bot 24/7 you can install OKE as a service for
the systemd daemon. You can do this by running the okcfg program which let's you 
enter <server> <channel> <nick> <modules> <owner> on the command line:

::

 > sudo okcfg localhost \#okbot okbot ~bart@127.0.0.1

if you don't want the bot to startup at boot, remove the service file:

::

 > sudo rm /etc/systemd/system/okbot.service 


C O M M A N D L I N E
=====================

OKBOT has it's own CLI called oksh, you can run it by oksh bot on the
prompt, it will return with its own prompt:

::

 > sudo oksh
 > cmds
 cfg|cmds|fleet|mods|ps|up|v

you can provide okcmd with an argument it will run the bot command directly:

::

 > sudo okcmd cmds
 cfg|cmds|ed|fleet|mods|ps|up|v

R S S
=====

the rss plugin uses the feedparser package, you need to install that yourself:

::

 > pip3 install feedparser

starts the rss fetcher with -m bot.rss.

to add an url use the rss command with an url:

::

 > okcmd rss https://news.ycombinator.com/rss
 ok 1

run the rss command to see what urls are registered:

::

 > okcmd rss
 0 https://news.ycombinator.com/rss

the fetch command can be used to poll the added feeds:

::

 > okcmd fetch
 fetched 0

U D P
=====

using udp to relay text into a channel, use the okudp program to send text via the bot 
to the channel on the irc server:

::

 > tail -f /var/log/syslog | okudp 

to send the tail output to the IRC channel, send a udp packet to okbot:

::

 import socket

 def toudp(host=localhost, port=5500, txt=""):
     sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
     sock.sendto(bytes(txt.strip(), "utf-8"), host, port)

C O D I N G
===========

.. _source:

OKE contains the following modules:

::

    okbot.base		- package base
    okbot.clk           - clock
    okbot.csl           - console 
    okbot.dft	        - default
    okbot.ed		- editor
    okbot.ent		- log,todo
    okbot.flt           - fleet
    okbot.fnd		- search objects
    okbot.gnr		- generic
    okbot.hdl           - handler
    okbot.irc      	- irc bot
    okbot.krn           - core handler
    okbot.rss           - rss to channel
    okbot.shl           - shell
    okbot.shw           - show runtime
    okbot.thr           - threads
    okbot.tms           - times
    okbot.trc           - trace
    okbot.typ           - types
    okbot.udp           - udp to channel
    okbot.usr           - users

C O M M A N D S
===============

basic code is a function that gets an event as a argument:

::

 def command(event):
     << your code here >>

to give feedback to the user use the event.reply(txt) method:

::

 def command(event):
     event.reply("yooo %s" % event.origin)


You can add you own modules to the botd package and if you want you can
create your own package with commands in the botd namespace.


have fun coding ;]

| 

C O N T A C T
=============

you can contact me on IRC/freenode/#dunkbots or email me at bthate@dds.nl

| Bart Thate (bthate@dds.nl, thatebart@gmail.com)
| botfather on #dunkbots irc.freenode.net
