# OKBOT - the ok bot !
#
# okbot.csl.py (console)

import sys, threading

from .krn import get_kernel
from .hdl import Command, Handler, dispatch
from .thr import launch

k = get_kernel()

class Console(Handler):

    def __init__(self):
        super().__init__()
        self._connected = threading.Event()
        self._ready = threading.Event()
        self._threaded = False
        
    def announce(self, txt):
        self.raw(txt)

    def poll(self):
        self._connected.wait()
        return Command(input("> "), repr(self), "root@shell")

    def input(self):
        import okbot.krn
        while not self._stopped:
            try:
                e = self.poll()
            except EOFError:
                break
            if not e.txt:
                continue
            dispatch(k, e)
        self._ready.set()

    def raw(self, txt):
        sys.stdout.write(str(txt) + "\n")
        sys.stdout.flush()

    def say(self, channel, txt, type="chat"):
        self.raw(txt)

    def start(self, handler=False, input=True):
        if self.error:
            return
        super().start(handler)
        if input:
            launch(self.input)
        self._connected.set()

    def wait(self):
        if self.error:
            return
        self._ready.wait()
