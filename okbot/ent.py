# OKBOT - the ok bot !
#
#

from okbot.base import Object
from okbot.db import Db

def __dir__():
    return ("Log", "Todo", 'done', 'log', 'todo')

class Log(Object):

    def __init__(self):
        super().__init__()
        self.txt = ""

class Todo(Object):

    def __init__(self):
        super().__init__()
        self.txt = ""

def log(event):
    if not event.rest:
       db = Db()
       nr = 0
       for o in db.find("okbot.ent.Log", {"txt": ""}):
            event.display(o, str(nr), strict=True)
            nr += 1
       return
    o = Log()
    o.txt = event.rest
    o.save()
    event.reply("ok")

def todo(event):
    if not event.rest:
       db = Db()
       nr = 0
       for o in db.find("okbot.ent.Todo", {"txt": ""}):
            event.display(o, str(nr), strict=True)
            nr += 1
       return
    o = Todo()
    o.txt = event.rest
    o.save()
    event.reply("okbot")

def done(event):
    if not event.args:
        event.reply("done <match>")
        return
    selector = {"txt": event.args[0]}
    got = []
    db = Db()
    for todo in db.find("okbot.ent.Todo", selector):
        todo._deleted = True
        todo.save()
        event.reply("okbot")
        break
