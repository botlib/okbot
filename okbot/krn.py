# OKBOT - the ok bot !
#
#

import inspect, sys, threading, time, _thread

from .base import Cfg, Object
from .db import Db
from .flt import Fleet
from .hdl import Command, Handler, dispatch
from .shl import writepid
from .usr import Users

def __dir__():
    return ("Cfg", "Kernel")

class Cfg(Cfg):

    pass

class Kernel(Handler):

    def __init__(self):
        super().__init__()
        self._outputed = False
        self._prompted = threading.Event()
        self._prompted.set()
        self._ready = threading.Event()
        self._started = False
        self.cfg = Cfg()
        self.db = Db()
        self.fleet = Fleet()
        self.users = Users()
        kernels.append(self)

    def add(self, cmd, func):
        self.cmds[cmd] = func

    def cmd(self, txt):
        e = Command(txt, repr(self), "root@shell")
        dispatch(self, e)

    def start(self):
        writepid()
        super().start()
            
    def ready(self):
        self._ready.set()

    def stop(self):
        self._stopped = True
        self._queue.put(None)

    def wait(self):
        self._ready.wait()

kernels = []

def get_kernel(nr=0):
    try:
        return kernels[nr]
    except IndexError:
        return Kernel()

def cmd(txt):
    k = get_kernel()
    return k.cmd(txt)
