# OKBOT - the ok bot !
#
#

import os

from okbot.base import Object, workdir
from okbot.db import Db
from okbot.krn import get_kernel
from okbot.utl import cdir

def __dir__():
    return ("fnd",)

k = get_kernel()

def fnd(event):
    if not event.args:
        wd = os.path.join(workdir, "store", "")
        cdir(wd)
        fns = os.listdir(wd)
        fns = sorted({x.split(os.sep)[0].split(".")[-1].lower() for x in fns})
        if fns:
            event.reply("|".join(fns))
        return
    shorts = k.find_shorts("okbot")
    db = Db()
    otypes = []
    target = db.all
    otype = event.args[0]
    otypes = shorts.get(otype, [otype,])
    try:
       match = event.args[1]
       target = db.find_value
    except:
       match = None
    try:
        args = event.args[2:]
    except ValueError:
        args = None
    nr = -1
    for ot in otypes:
        for o in target(ot, match):
            nr += 1
            event.display(o, str(nr), args or o.keys())
    if nr == -1:
        event.reply("no %s objects." % "|".join(otypes))
