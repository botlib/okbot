# OKBOT - the ok bot !
#
#

import importlib, inspect, os, pkg_resources, queue
import sys, threading, time, types, _thread
import okbot.tbl

from .base import Cfg, Default, DoL, Object
from .tms import elapsed
from .thr import launch
from .trc import get_exception
from .tms import fntime

def __dir__():
    return ("Cfg", "Event", "Handler", "Loader", "dispatch")

def dispatch(handler, event):
    func = handler.cmds.get(event.cmd)
    if not func:
        mn = okbot.tbl.names.get(event.cmd)
        if mn:
            handler.load_mod(mn)
    func = handler.cmds.get(event.cmd)
    if not func:
        return
    event._func = func
    func(event)
    event.show()
    event.ready()

class Cfg(Cfg):
    pass

class Loader(Object):

    table = Object()

    def __init__(self):
        super().__init__()
        self.cmds = Object()
        self.mods = Object()
        self.error = ""
                
    def direct(self, name):
        return importlib.import_module(name)

    def find_callbacks(self, mod):
        cbs = {}
        for key, o in inspect.getmembers(mod, inspect.isfunction):
            if o.__code__.co_argcount == 2:
                cbs[key] = o
        return cbs

    def find_cmds(self, mod):
        cmds = {}
        for key, o in inspect.getmembers(mod, inspect.isfunction):
            if "event" in o.__code__.co_varnames:
                if o.__code__.co_argcount == 1:
                    cmds[key] = o
        return cmds

    def find_modules(self, mns):
        for mn in mns.split(","):
            mod = self.direct(mn)
            mods = []
            for key, o in inspect.getmembers(mod, inspect.ismodule):
                if o not in mods:
                    mods.append(o)
        return mods

    def find_names(self, mod):
        names = {}
        for key, o in inspect.getmembers(mod, inspect.isfunction):
            if "event" in o.__code__.co_varnames:
                if o.__code__.co_argcount == 1:
                    names[key] = o.__module__
        return names

    def find_shorts(self, mn):
        shorts = DoL()
        for mod in self.find_modules(mn):
            for key, o in inspect.getmembers(mod, inspect.isclass):
                if issubclass(o, Object):
                    t = "%s.%s" % (o.__module__, o.__name__)
                    shorts.append(o.__name__.lower(), str(t))
        return shorts

    def find_types(self, mn):
        res = []
        for mod in self.find_modules(mn):
            for key, o in inspect.getmembers(mod, inspect.isclass):
                if issubclass(o, Object):
                    t = "%s.%s" % (o.__module__, o.__name__)
                    res.append(t)
        return res

    def load_mod(self, mn):
        if mn in Loader.table:
            return Loader.table[mn]
        Loader.table[mn] = self.direct(mn)
        self.scan(Loader.table[mn])
        return Loader.table[mn]

    def walk(self, mns):
        mods = []
        for mn in mns.split(","):
            if not mn:
                continue
            try:
                fns = pkg_resources.resource_listdir(mn, "")
            except TypeError:
                mod = self.direct(mn)
                try:
                    fns = list(mod.__path__)
                except AttributeError:
                    fns = [mod.__file__,]
            for x in fns:
                for xx in os.listdir(x):
                    if xx.startswith("_") or not xx.endswith(".py"):
                        continue
                    mmn = "%s.%s" % (mn, xx[:-3])
                    module = self.load_mod(mmn)
                    mods.append(module)
        return mods

    def scan(self, mod):
        cmds = self.find_cmds(mod)
        self.cmds.update(cmds)
        modules = self.find_names(mod)
        self.mods.update(modules)
        return cmds

class Handler(Loader):
 
    def __init__(self):
        super().__init__()
        self._queue = queue.Queue()
        self._closed = threading.Event()
        self._stopped = False
        self.cbs = Object()
        self.outcache = DoL()
        self.register("command", dispatch)

    def handle_cb(self, event):
        if event.etype in self.cbs:
            self.cbs[event.etype](self, event)
        event.ready()

    def handler(self):
        while not self._stopped:
            e = self._queue.get()
            if e == None:
                break
            self.handle_cb(e)
        self._closed.set()

    def poll(self):
        raise ENOTIMPLEMENTED

    def put(self, event):
        self._queue.put_nowait(event)

    def register(self, cbname, handler):
        self.cbs[cbname] = handler        

    def start(self, handler=True):
        if handler:
            launch(self.handler)

    def stop(self):
        self._stopped = True
        self._queue.put(None)
        
    def wait(self, nrsec):
        while not self._stopped:
            time.sleep(nrsec)

class Event(Default):

    def __init__(self, txt="", orig=None, origin="root@shell"):
        super().__init__()
        self._error = ""
        self._ready = threading.Event()
        self._result = []
        self._thrs = []
        self.etype = "event"
        self.txt = txt
        self.orig = orig
        self.origin = origin
        self.parse()
        
    def display(self, o, txt="", keys=None, options="t", post="", strict=False):
        if not keys:
            keys = list(o.keys())
        txt = txt[:]
        txt += " %s" % self.format(o, keys, strict=strict) 
        if "t" in options:
           txt += " %s" % elapsed(time.time() - fntime(o._path))
        if post:
           txt += " " + post
        txt = txt.strip()
        self.reply(txt)

    def format(self, o, keys=None, strict=False):
        if keys is None:
            keys = list(vars(o).keys())
        res = []
        txt = ""
        for key in keys:
            val = o.get(key)
            if not val:
                continue
            val = str(val)
            if key == "text":
                val = val.replace("\\n", "\n")
            res.append((key, val))
        for key, val in res:
            if strict:
                txt += "%s%s" % (val.strip(), " ")
            else:
                txt += "%s=%s%s" % (key, val.strip(), " ")
        return txt.strip()

    def missing(self, txt):
        self.reply(txt)
        self.ready()

    def parse(self):
        spl = self.txt.split()
        if spl:
            self.cmd = spl[0].lower()
            self.args = spl[1:]
            self.rest = " ".join(self.args)
                
    def ready(self):
        self._ready.set()

    def reply(self, txt):
        self._result.append(txt)
 
    def show(self):
        for txt in self._result:
            if self.fleet:
                self.fleet.say(self.orig, self.channel, txt)
            else:
                print(txt)

    def wait(self, nrsec=None):
        self._ready.wait(nrsec)

class Command(Event):

    def __init__(self, txt, orig, origin):
        super().__init__(txt, orig, origin)
        self.etype = "command"
